import Cookies from 'js-cookie';
import {isEmpty} from './base';

let auth = {};

auth.setUser = function () {
    return Cookies.get(tokenKey);
};

const tokenKey = 'token_key';

auth.getToken = function () {
    return Cookies.get(tokenKey);
};

auth.setToken = function (token) {
    return Cookies.set(tokenKey, token);
};

auth.removeToken = function (token) {
    return Cookies.remove(tokenKey);
};

auth.isLogin = function () {
    var isLogin = false;
    var token = auth.getToken();
    isLogin = !isEmpty(token);
    return isLogin;
};

auth.isLocking = function () {
    var locking = Cookies.get('locking') === '1';
    return locking;
};

export default auth;
