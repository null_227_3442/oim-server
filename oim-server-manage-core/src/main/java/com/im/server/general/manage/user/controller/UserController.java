package com.im.server.general.manage.user.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.onlyxiahui.common.message.result.ResultMessage;
import com.onlyxiahui.general.annotation.parameter.RequestParameter;

/**
 * date 2018-06-04 11:47:27<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@RequestMapping("/manage/user")
public class UserController {

	@CrossOrigin
	@ResponseBody
	@RequestParameter
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public Object list() {
		ResultMessage rm = new ResultMessage();
		rm.addError("500", "系统异常");
		return rm;
	}
}
