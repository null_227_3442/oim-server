package com.im.server.general.manage.dao;

import java.util.UUID;

import com.onlyxiahui.query.mybatis.dao.GenericDAO;

/**
 * 
 * @author: XiaHui
 *
 */
public class BaseDAO {

	@org.springframework.beans.factory.annotation.Autowired(required = false)
	protected GenericDAO writeDAO;

	@org.springframework.beans.factory.annotation.Autowired(required = false)
	protected GenericDAO readDAO;

	public String getId() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
