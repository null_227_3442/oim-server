package com.im.server.general.manage.index.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.onlyxiahui.common.message.result.ResultMessage;
import com.onlyxiahui.general.annotation.parameter.RequestParameter;

/**
 * date 2018-06-04 11:47:27<br>
 * description 
 * @author XiaHui<br>
 * @since 
 */
@RequestMapping("/manage/index")
public class IndexController {
	
	@CrossOrigin
	@ResponseBody
	@RequestParameter
	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public Object login() {
		ResultMessage rm = new ResultMessage();
		rm.addError("500", "系统异常");
		return rm;
	}
}
